export LS_OPTIONS='--color=auto'
eval "`dircolors`"
alias ls="ls $LS_OPTIONS"
force_color_prompt=yes
export TERM=xterm-256color
export PS1="\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\[\033[01;38;5;130m\]\u@\h\[\033[00m\]:\[\033[01;38;5;240m\]\w\[\033[00m\]\$ "
