#!/bin/bash

for pkg in clang ll; do

  bins=`find /usr/bin -name "$pkg*"`

  for x in $bins; do
    name=`echo $x | xargs -n 1 basename | sed 's/-9//g'`
    update-alternatives --install /usr/bin/$name $name $x 100
  done

done
