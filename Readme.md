# Builder

A container designed to support local development. To customize this container
you should derive from it. If you have an addition that you think will be
**generally useful** to the base or a new derivative container, merge requests 
are welcome.

## Current Containers

|Container|Purpose|
|---|---|
| base | A basic build environment with essential tools |
| kernel | A build environment for kernel development |

## Usage

The most basic way to use the builder container is the following.

```shell
docker run -it mergetb/builder -h builder
```

However, most commonly you'll want to mount your code into the builder for
building. Building a project named lambda may look like the following.

```shell
docker run -v `pwd`:/lambda -it mergetb/builder -h lambda🔨
```
