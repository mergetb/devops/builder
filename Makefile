.PHONY: all
all: base f39-cib f40-cib f41-cib centoss8-cib centoss9-cib centoss10-cib almalinux9-cib

.PHONY: base
base: base.dock
	$(call podman-build,builder)

.PHONY: f39-cib
f39-cib: f39-cib.dock
	$(call podman-build,f39-cib)

.PHONY: f40-cib
f40-cib: f40-cib.dock
	$(call podman-build,f40-cib)

.PHONY: f41-cib
f41-cib: f41-cib.dock
	$(call podman-build,f41-cib)

.PHONY: centoss8-cib
centoss8-cib: centoss8-cib.dock
	$(call podman-build,centoss8-cib)

.PHONY: centoss9-cib
centoss9-cib: centoss9-cib.dock
	$(call podman-build,centoss9-cib)

.PHONY: centoss10-cib
centoss10-cib: centoss10-cib.dock
	$(call podman-build,centoss10-cib)

.PHONY: almalinux9-cib
almalinux9-cib: almalinux9-cib.dock
	$(call podman-build,almalinux9-cib)

.PHONY: container
REGISTRY ?= registry.gitlab.com
ORG      ?= mergetb/devops/builder
TAG      ?= latest

QUIET=@
ifeq ($(V),1)
	QUIET=
endif

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[0m

define build-slug
	@echo -e "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define podman-build
	$(call build-slug,podman)
	$(QUIET) podman build \
		${BUILD_ARGS} $(DOCKER_QUIET) -f $< \
		-t $(REGISTRY)$(REGPORT)/$(ORG)/$1:$(TAG) .
	$(if ${PUSH},$(call podman-push,$1))
endef

define podman-push
	$(call build-slug,push)
	$(QUIET) podman push $(REGISTRY)$(REGPORT)/$(ORG)/$1:$(TAG)
endef
